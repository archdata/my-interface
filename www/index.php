<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Atmospheric Sensors</title>
    
    <link href="/css/reset.css" rel="stylesheet" type="text/css">
    <link href="/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="/css/buttons.css" rel="stylesheet" type="text/css">
    <link href="/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/css/main.css" rel="stylesheet" type="text/css">
    
    <script language="javascript" type="text/javascript" src="/js/libraries/jquery-1.9.0.js"></script>
    <script language="javascript" type="text/javascript" src="/js/libraries/flot/jquery.flot.js"></script>
    <script language="javascript" type="text/javascript" src="/js/libraries/flot/jquery.flot.time.js"></script>
    <script language="javascript" type="text/javascript" src="/js/libraries/flot/jquery.flot.resize.min.js"></script>
    <script language="javascript" type="text/javascript" src="/js/main.js"></script>
<!--     <script language="javascript" type="text/javascript" src="/js/websockets.js"></script> -->
</head>
<body>

    <header>
        <h1>Atmospheric Sensors</h1>
    </header>

    <div id="content">
        
        <div id="sensor_01" class="sensor"> 
            <div class="chart_wrap">
                <h2>Temperature</h2>
                <div id="temp_current" class="current_stats"></div>
                <div class="chart">
                    <div id="temperature"></div>
                </div>
            </div>
            
            <div class="chart_wrap">
                <h2>Humidity</h2>
                <div id="humid_current" class="current_stats"></div>
                <div class="chart">
                    <div id="humidity"></div>
                </div>
            </div>

            <div class="chart_wrap">
                <h2>Pressure</h2>
                <div id="pressure_current" class="current_stats"></div>
                <div class="chart">
                    <div id="pressure"></div>
                </div>
            </div>
<!-- 

            <div class="chart_wrap">
                <h2>Vibration</h2>
                <div id="incident" class="current_stats"><select><option>...</option></select></div>
                <div class="chart">
                    <div id="vibration">           
                        <div id="vibration_chart" class="chart_holder"></div>       
                    </div>
                </div<
            </div>
 -->

        </div>

    </div>

    <footer>
    </footer>

</body>
</html>
