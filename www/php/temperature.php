<?php 

include('../config/config.php');
include('../config/db.php');

$temperatureQuery = "SELECT UNIX_TIMESTAMP(timestamp), temperature FROM sensor_01 WHERE timestamp > DATE_ADD(NOW(), INTERVAL -26 HOUR) ORDER BY timestamp ASC";

//$temperatureQuery = "SELECT timestamp, temperature FROM sensor_01 ORDER BY timestamp DESC LIMIT 4320";

$output = mysqli_query($dbConnection, $temperatureQuery) or die(mysqli_error());

while ($row = mysqli_fetch_array($output)) {
    
    $results[] = array(($row[0]*1000), floatval($row[1]));

};

echo json_encode($results);


?>