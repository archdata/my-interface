<?php 

include('../config/config.php');
include('../config/db.php');

$humidityQuery = "SELECT UNIX_TIMESTAMP(timestamp), humidity FROM sensor_01 WHERE timestamp > DATE_ADD(NOW(), INTERVAL -26 HOUR) ORDER BY timestamp ASC";

$output = mysqli_query($dbConnection, $humidityQuery) or die(mysqli_error());

while ($row = mysqli_fetch_array($output)) {
    
    $results[] = array(($row[0]*1000), floatval($row[1]));

};

echo json_encode($results);


?>