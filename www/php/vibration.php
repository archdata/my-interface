<?php 

include('../config/config.php');
include('../config/db.php');

$humidityQuery = "SELECT incident_time, xaxis, yaxis, zaxis FROM sensor_01_vibration ORDER BY id ASC";

$output = mysqli_query($dbConnection, $humidityQuery) or die(mysqli_error());

$incident_time = '';
$data = [];

while ($row = mysqli_fetch_array($output)) {
    
    $results[] = array($row[0], $row[1], $row[2], $row[3]);    
    
}

for ($i = 0; $i < count($results); $i++) {

    if ($results[$i][1] == 100) {
        $incident_time = $results[$i+1][0];
        $data[$incident_time] = [];
    }
    
    if ($results[$i][0] != $incident_time) {
        if ($results[$i][1] != 100) {
            array_push($data[$incident_time], $results[$i]);
        }
    }    

}


// $json_string = json_encode($data, JSON_PRETTY_PRINT);
$json_string = json_encode($data);

// echo "<pre>";
echo $json_string;
// echo "</pre>";

?>