<?php 

include('../config/config.php');
include('../config/db.php');

$temperatureQuery = "SELECT ROUND(avg(humidity),1) as 'Median Humidity'
FROM ( select humidity
      FROM sensor_01
      GROUP BY humidity
      HAVING sum(case when sensor_01.humidity = sensor_01.humidity then 1 else 0 end)
                     >= abs(sum(sign(sensor_01.humidity - sensor_01.humidity)))
      ) t";

$output = mysqli_query($dbConnection, $temperatureQuery) or die(mysqli_error());

while ($row = mysqli_fetch_array($output)) {
    
    $results[] = $row[0];

};

echo json_encode($results);


?>