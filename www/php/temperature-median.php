<?php 

include('../config/config.php');
include('../config/db.php');

$temperatureQuery = "SELECT ROUND(avg(temperature),1) as 'Median Temperature'
FROM ( select temperature
      FROM sensor_01
      GROUP BY temperature
      HAVING sum(case when sensor_01.temperature = sensor_01.temperature then 1 else 0 end)
                     >= abs(sum(sign(sensor_01.temperature - sensor_01.temperature)))
      ) t";

$output = mysqli_query($dbConnection, $temperatureQuery) or die(mysqli_error());

while ($row = mysqli_fetch_array($output)) {
    
    $results[] = $row[0];

};

echo json_encode($results);


?>