$(document).ready(function() {
// Start websockets.js
// ***************************************

websocket = new WebSocket("ws://127.0.0.1:9998/");

websocket.onopen = function() {

    console.log("Socket Open");

};

websocket.onmessage = function (evt) { 

    var received_msg = evt.data;
    var received_msg = JSON.parse(evt.data);
//     
//     if (received_msg.Message == "reload") {
//             
//             window.location.reload()
//             
//     }
    
    $.getVibrationData(received_msg.Message[0])


};

websocket.onclose = function() { 

    console.log("Socket Closed"); 

};


function stripTrailingSlash(str) {
    if(str.substr(-1) == '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
}



// ***************************************
// End websockets.js
});

