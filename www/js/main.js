$(document).ready(function() {
// Start main.js
// **********************************



var data = []

var temperature_options  = {
    grid: {
        labelMargin: 10
    },
    series: {
        shadowSize: 4,
        lines: {
            lineWidth: 2
        },
        color: "rgba(215, 44, 44, 0.8)"
    },
    yaxis: {
        min: 0,
        max: 40,
        show: true,
        font: {
             size: 10,
             weight: "bold",
             family: "sans-serif",
             color: "black"
        },
    },
    xaxis: {
        mode: "time",
        timezone: 'browser',
        font: {
             size: 10,
             weight: "bold",
             family: "sans-serif",
             color: "black"
        },
    }
}

var humidity_options  = {
    grid: {
        labelMargin: 10
    },
    series: {
        shadowSize: 4,
        lines: {
            lineWidth: 2
        },
        color: "rgba(78, 92, 255, 0.8)"
    },
    yaxis: {
        min: 0,
        max: 100,
        show: true,
        font: {
             size: 10,
             weight: "bold",
             family: "sans-serif",
             color: "black"
        },
    },
    xaxis: {
        mode: "time",
        timezone: 'browser',
        font: {
             size: 10,
             weight: "bold",
             family: "sans-serif",
             color: "black"
        },
    }
}

var pressure_options  = {
    grid: {
        labelMargin: 10
    },
    series: {
        shadowSize: 4,
        lines: {
            lineWidth: 2
        },
        color: "rgba(78, 150, 100, 0.8)"
    },
    yaxis: {
	min: 98,
	max: 105,
        show: true,
        font: {
             size: 10,
             weight: "bold",
             family: "sans-serif",
             color: "black"
        },
    },
    xaxis: {
        mode: "time",
        timezone: 'browser',
        font: {
             size: 10,
             weight: "bold",
             family: "sans-serif",
             color: "black"
        },
    }
}

// var vibration_options  = {
//     
//     series: {
//         shadowSize: 0,
//         color: "rgba(78, 150, 100, 0.8)"
//     },
//     yaxis: {
//         min: -4,
//         max: 4,
//         show: true
//     },
//     xaxis: {
//         mode: "time",
//         timezone: 'browser'
//     }
// }

var plot_temp = $.plot("#sensor_01 #temperature", [data], temperature_options);
var plot_humid = $.plot("#sensor_01 #humidity", [data], humidity_options);
var plot_pressure = $.plot("#sensor_01 #pressure", [data], pressure_options);
// var plot_vibration = $.plot("#sensor_01 #vibration", [data], vibration_options);

function getTempData() {
    
    $.getJSON('/php/temperature.php').done(function(data) {
        

        var temperature_dataset = [
            {
                data: data
            }
        ];

        plot_temp.setData(temperature_dataset);
        plot_temp.setupGrid();
        plot_temp.draw();

    });

};

function getHumidData() {
    
    $.getJSON('/php/humidity.php').done(function(data) {
        

        var humidity_dataset = [
            {
                data: data
            }
        ];

        plot_humid.setData(humidity_dataset);
        plot_humid.setupGrid();
        plot_humid.draw();

    });

};

function getPressureData() {
    
    $.getJSON('/php/pressure.php').done(function(data) {
        
        var pressure_dataset = [
            {
                data: data
            }
        ];

        plot_pressure.setData(pressure_dataset);
        plot_pressure.setupGrid();
        plot_pressure.draw();

    });

};

function getVibrationData() {

    var tag = '';

    $.getJSON('/php/vibration.php').done(function(data) {
        
        vibration_results = data;
        
        tag = "<option>...</option>"
        
        $.each(data, function( index, value ){
            var d2 = Date.createFrommysqli(index);
            tag += '<option value="' + index + '">' + index + '</option>';
        });


        $('#incident select').html(tag);


    });

};

Date.createFrommysqli = function(mysqli_string) { 
    if (typeof mysqli_string === 'string') {

        var t = mysqli_string.split(/[- : .]/);
        var miliseconds = t[6]
        //when t[3], t[4] and t[5] are missing they defaults to zero
        returnDate = (new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0).getTime() / 1000) + miliseconds;          
        return returnDate
    }

    return null;   
}


$('#incident select').on('change', function(){
    
    value = $(this).val();
        
    xValues = [];    
    yValues = [];
    zValues = [];
        
    current_data = vibration_results[value]
    data_length = (current_data).length;
    
    for (i = 0; i < data_length; i++) { 

        xValues[i] = [ Date.createFrommysqli(current_data[i][0]), current_data[i][1] ];
        yValues[i] = [ Date.createFrommysqli(current_data[i][0]), current_data[i][2] ];
        zValues[i] = [ Date.createFrommysqli(current_data[i][0]), current_data[i][3] ];

    }

    var vibration_dataset = [
        { label: "X", data: xValues, lines:{show:true}, color: "rgba(255, 150, 100, 0.8)"},
        { label: "Y", data: yValues, lines:{show:true}, color: "rgba(78, 255, 255, 0.8)"},
        { label: "Z", data: zValues, lines:{show:true}, color: "rgba(100, 5, 128, 0.2)"}
    ];

    plot_vibration.setData(vibration_dataset);
    plot_vibration.setupGrid();
    plot_vibration.draw();
    
})

function getTempCurrent() {

    $.getJSON('/php/temperature-current.php').done(function(data) {
        
        $('#temp_current').html(data[0] + '&#8201;&#186;C');

    });
}

function getHumidCurrent() {

    $.getJSON('/php/humidity-current.php').done(function(data) {
        
        $('#humid_current').html(data[0] + '&#8201;%');

    });
}

function getPressureCurrent() {

    $.getJSON('/php/pressure-current.php').done(function(data) {
        
        $('#pressure_current').html(data[0] + ' kPa');

    });
}


function run_at_interval() {
    
    getTempData();
    getHumidData();
    getPressureData();
    getHumidCurrent();
    getTempCurrent();
    getPressureCurrent();

}

getVibrationData();
run_at_interval();
setInterval(run_at_interval, 30000);

// **********************************
// End main.js
});
